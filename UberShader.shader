Shader "hidden/preview"
{
    Properties
    {
        [NoScaleOffset] Texture_976DA502("Texture", 2D) = "white" {}
    }
    HLSLINCLUDE
    #define USE_LEGACY_UNITY_MATRIX_VARIABLES
    #include "CoreRP/ShaderLibrary/Common.hlsl"
    #include "CoreRP/ShaderLibrary/Packing.hlsl"
    #include "CoreRP/ShaderLibrary/Color.hlsl"
    #include "CoreRP/ShaderLibrary/UnityInstancing.hlsl"
    #include "CoreRP/ShaderLibrary/EntityLighting.hlsl"
    #include "ShaderGraphLibrary/ShaderVariables.hlsl"
    #include "ShaderGraphLibrary/ShaderVariablesFunctions.hlsl"
    #include "ShaderGraphLibrary/Functions.hlsl"
    float Vector1_877D1635;
    TEXTURE2D(Texture_976DA502); SAMPLER(samplerTexture_976DA502);
    float4 _SampleTexture2D_6C59550C_UV;
    struct SurfaceInputs{
    	half4 uv0;
    };
    struct GraphVertexInput
    {
    	float4 vertex : POSITION;
    	float3 normal : NORMAL;
    	float4 tangent : TANGENT;
    	float4 texcoord0 : TEXCOORD0;
    	UNITY_VERTEX_INPUT_INSTANCE_ID
    };
    struct SurfaceDescription{
    	float4 PreviewOutput;
    };
    GraphVertexInput PopulateVertexData(GraphVertexInput v){
    	return v;
    }
    SurfaceDescription PopulateSurfaceData(SurfaceInputs IN) {
    	SurfaceDescription surface = (SurfaceDescription)0;
    	float4 _SampleTexture2D_6C59550C_RGBA = SAMPLE_TEXTURE2D(Texture_976DA502, samplerTexture_976DA502, IN.uv0.xy);
    	float _SampleTexture2D_6C59550C_R = _SampleTexture2D_6C59550C_RGBA.r;
    	float _SampleTexture2D_6C59550C_G = _SampleTexture2D_6C59550C_RGBA.g;
    	float _SampleTexture2D_6C59550C_B = _SampleTexture2D_6C59550C_RGBA.b;
    	float _SampleTexture2D_6C59550C_A = _SampleTexture2D_6C59550C_RGBA.a;
    	if (Vector1_877D1635 == 2) { surface.PreviewOutput = half4(_SampleTexture2D_6C59550C_RGBA.x, _SampleTexture2D_6C59550C_RGBA.y, _SampleTexture2D_6C59550C_RGBA.z, 1.0); return surface; }
    	return surface;
    }
    ENDHLSL

    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            HLSLPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            struct GraphVertexOutput
            {
                float4 position : POSITION;
                half4 uv0 : TEXCOORD;

            };

            GraphVertexOutput vert (GraphVertexInput v)
            {
                v = PopulateVertexData(v);

                GraphVertexOutput o;
                float3 positionWS = TransformObjectToWorld(v.vertex);
                o.position = TransformWorldToHClip(positionWS);
                o.uv0 = v.texcoord0;

                return o;
            }

            float4 frag (GraphVertexOutput IN) : SV_Target
            {
                float4 uv0 = IN.uv0;


                SurfaceInputs surfaceInput = (SurfaceInputs)0;;
                surfaceInput.uv0 = uv0;


                SurfaceDescription surf = PopulateSurfaceData(surfaceInput);
                return surf.PreviewOutput;

            }
            ENDHLSL
        }
    }
}
