﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider))]

public class Item : MonoBehaviour {

    public string ItemID;

    public ComponentManager.ItemDataClass Data = new ComponentManager.ItemDataClass();
    public int Quantity = 1;

    public int OwnerIndex;

    bool initialized;
    void Update()
    {
        if(ComponentManager.DataLoaded && !initialized)
        {
            Initialize();
        }

    }
    private void Initialize()
    {
        initialized = true;
        Data = ComponentManager.GetItemData(ItemID);

        if(Data == null)
        {
            Debug.Log("No data for this object so disabling it ", this.gameObject);
            enabled = true;
        }
        else
        {
            if (this.GetComponent<BoxCollider>().isTrigger != true)
            {
                Debug.Log("Setting collider to trigger for " + this.gameObject.name, this.gameObject);
                this.GetComponent<BoxCollider>().isTrigger = true;
            }
        }        
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.GetComponent<Collider>().CompareTag("Player"))
        {
            other.GetComponent<PlayerCharacter>().CollectItem(Data, Quantity);
            Destroy(gameObject);
        }
    }

    public void Use(PlayerCharacter playerCharacter)
    {
        switch (Data.ItemType)
        {
            case "Default":               
                break;
            case "Axe":
                playerCharacter.ModelAnimator.SetTrigger("ChopTree");
                AudioManager.instance.PlaySFX(AudioManager.instance.Sounds.ToolSwing);
                break;
            case "Hoe":
                playerCharacter.ModelAnimator.SetTrigger("HoeGround");
                AudioManager.instance.PlaySFX(AudioManager.instance.Sounds.HoeGround);
                if (playerCharacter.lastHighlightedTile != null)
                {
                    playerCharacter.lastHighlightedTile.GetComponent<TileToolTileExt>().Interact(TileInteractionType.Hoe);
                }
                break;
            default:
                break;
        }
    }
}
