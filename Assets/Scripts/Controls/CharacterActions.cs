﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using InControl;

public class CharacterActions : PlayerActionSet {

    public bool AreEnabled = true;

    public PlayerAction Left;
    public PlayerAction Right;
    public PlayerAction Up;
    public PlayerAction Down;
    public PlayerTwoAxisAction Move;

    public PlayerAction Interact;
    public PlayerAction Inventory;
    public PlayerAction QuickBarRight;
    public PlayerAction QuickBarLeft;


    public CharacterActions(InputDevice inputDevice)
    {
        Device = inputDevice;

        Left = CreatePlayerAction("Move Left");
        Right = CreatePlayerAction("Move Right");
        Up = CreatePlayerAction("Move Up");
        Down = CreatePlayerAction("Move Down");
        Move = CreateTwoAxisPlayerAction(Left, Right, Down, Up);

        Interact = CreatePlayerAction("Interact");

        Inventory = CreatePlayerAction("Inventory");
        QuickBarRight = CreatePlayerAction("QuickBarRight");
        QuickBarLeft = CreatePlayerAction("QuickBarLeft");
    }
}
