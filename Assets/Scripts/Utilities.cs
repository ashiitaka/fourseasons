﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Utilities {

    public static void SelectObject(GameObject selection)
    {
        if (selection.GetComponent<Selectable>())
        {
            selection.GetComponent<Selectable>().Select();
        }

        if (selection.GetComponent<Button>())
        {
            selection.GetComponent<Button>().OnSelect(null);
        }

        if (selection.GetComponent<UIButtonResponder>())
        {
            selection.GetComponent<UIButtonResponder>().OnSelect(null);
        }
    }
}
