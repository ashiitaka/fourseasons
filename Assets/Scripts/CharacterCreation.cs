﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterCreation : MonoBehaviour {

    public static CharacterCreation instance;

    [SerializeField]
    private GameObject CharacterCustomizationParent;
    [SerializeField]
    private Transform CharacterCustomizationSpawnLocation;
    [SerializeField]
    private Character BaseCharacterPrefab;
    public PlayerCharacter tempCharacter;

    public enum componentType
    {
        Head,
        Body,
    }

    int currentHeadIndex = 0;
    int currentBodyIndex = 0;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }

        //If instance already exists and it's not this:
        else if (instance != this)

            //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
            Destroy(gameObject);

        CharacterCustomizationParent.SetActive(false);
    }

    public void ShowCharacterCreation(Player ControllingPlayer,PlayerCharacter editingCharacter, int headIndex, int bodyIndex)
    {
        CharacterCustomizationParent.SetActive(true);

        tempCharacter = editingCharacter;
        ControllingPlayer.BindControls(ControllingPlayer.PlayerController);
        ControllingPlayer.Controls.AreEnabled = false;

        tempCharacter.transform.SetParent(CharacterCustomizationSpawnLocation);
        tempCharacter.transform.localPosition = Vector3.zero;
        tempCharacter.transform.localRotation = Quaternion.identity;

        tempCharacter.CharacterComposition.Head = Instantiate(ComponentManager.Heads[headIndex], tempCharacter.HeadLocation);
        tempCharacter.CharacterComposition.Head.GetComponent<ComponentBase>().UpdateTextures(0);
        tempCharacter.CharacterComposition.Body = Instantiate(ComponentManager.Bodies[bodyIndex], tempCharacter.BodyLocation);
        tempCharacter.CharacterComposition.Body.GetComponent<ComponentBase>().UpdateTextures(0);
    }

    public void ConfirmCharacterCreation(Player player)
    {
        Character.Bodycomposition bodycomposition = new Character.Bodycomposition();
        bodycomposition.HeadIndex = currentHeadIndex;
        bodycomposition.HeadTextureIndex = 0;
        bodycomposition.BodyIndex = currentBodyIndex;
        bodycomposition.BodyTextureIndex = 0;

        player.AddSelectedCharacterToWorld(new Vector3(0, .5f, 0));
        //PlayerManager.instance.AddPlayer(tempCharacter, new Vector3(0,.5f,0)); //TODO drive this spawn position better
    }

    public enum NextOrPrevious
    {
        Next,
        Previous
    }
    public void ComponentCycle(CharacterCreation.componentType compType, NextOrPrevious direction)
    {
        switch (compType)
        {
            case componentType.Head:
                switch (direction)
                {
                    case NextOrPrevious.Next:
                        if (currentHeadIndex >= ComponentManager.Heads.Count - 1)
                        {
                            currentHeadIndex = 0;
                        }
                        else
                        {
                            currentHeadIndex++;
                        }
                        break;
                    case NextOrPrevious.Previous:
                        if (currentHeadIndex == 0)
                        {
                            currentHeadIndex = ComponentManager.Heads.Count - 1;
                        }
                        else
                        {
                            currentHeadIndex--;
                        }
                        break;
                    default:
                        break;
                }
                ChangeComponent(componentType.Head, currentHeadIndex);
                break;
            case componentType.Body:
                switch (direction)
                {
                    case NextOrPrevious.Next:
                        if (currentBodyIndex >= ComponentManager.Bodies.Count - 1)
                        {
                            currentBodyIndex = 0;
                        }
                        else
                        {
                            currentBodyIndex++;
                        }
                        break;
                    case NextOrPrevious.Previous:
                        if (currentBodyIndex == 0)
                        {
                            currentBodyIndex = ComponentManager.Bodies.Count - 1;
                        }
                        else
                        {
                            currentBodyIndex--;
                        }
                        break;
                    default:
                        break;
                }
                ChangeComponent(componentType.Body, currentBodyIndex);
                break;
            default:
                break;
        }
    }

    void ChangeComponent(CharacterCreation.componentType newType, int newIndex)
    {
        switch (newType)
        {
            case componentType.Head:
                Destroy(tempCharacter.CharacterComposition.Head);
                tempCharacter.CharacterComposition.Head = Instantiate(ComponentManager.Heads[currentHeadIndex], tempCharacter.HeadLocation);
                tempCharacter.CharacterComposition.Head.GetComponent<ComponentBase>().UpdateTextures(0);
                Debug.Log(currentHeadIndex);
                break;
            case componentType.Body:
                Destroy(tempCharacter.CharacterComposition.Body);
                tempCharacter.CharacterComposition.Body = Instantiate(ComponentManager.Bodies[currentBodyIndex], tempCharacter.BodyLocation);
                if(tempCharacter.CharacterComposition.Body.GetComponent<ComponentBase>() && tempCharacter.BodyMeshRenderer != null)
                {
                    if(tempCharacter.CharacterComposition.Body.GetComponent<ComponentBase>().MainObject.GetComponent<Equipmentizer>())
                    {
                        tempCharacter.CharacterComposition.Body.GetComponent<ComponentBase>().MainObject.GetComponent<Equipmentizer>().TargetMeshRenderer = tempCharacter.BodyMeshRenderer;
                        tempCharacter.CharacterComposition.Body.GetComponent<ComponentBase>().MainObject.GetComponent<Equipmentizer>().Equip();
                    }

                }
                tempCharacter.CharacterComposition.Body.GetComponent<ComponentBase>().UpdateTextures(0);
                Debug.Log(currentBodyIndex);
                break;
            default:
                break;
        }


    }
}
