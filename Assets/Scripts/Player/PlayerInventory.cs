﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInventory {

    public const int MaxInventorysize = 30;
    public const int QuickBarSize = 10;

    public class InventoryRef
    {
        public string ItemID;
        public int Quantity;

        public InventoryRef(string itemID, int quantity)
        {
            ItemID = itemID;
            Quantity = quantity;
        }
    }

    List<InventoryRef> Items = new List<InventoryRef>();

    public int EquippedQuickBarIndex;

    public InventoryBar QuickBarUI;

    public void Initialize()
    {
        for (int i = 0; i < MaxInventorysize; i++)
        {
            Items.Add(new InventoryRef("None",0));
        }
    }

    public List<InventoryRef> GetItemList()
    {
        return Items;
    }

    public PlayerInventory(InventoryBar inventoryBar)
    {
        QuickBarUI = inventoryBar;
    }

    public void AddItemToInventory(ComponentManager.ItemDataClass newItem, int quantity)
    {
        bool addedItem = false;
        for (int i = 0; i < Items.Count; i++)
        {
            if(Items[i].ItemID == "None")
            {
                Items[i].ItemID = newItem.ItemID;
                Items[i].Quantity = quantity;
                addedItem = true;
                break;
            }
            else if(Items[i].ItemID == newItem.ItemID)
            {
                Items[i].Quantity += quantity;
                addedItem = true;
                break;
            }
        }

        if(!addedItem)
        {
            Debug.Log("No room for new items!");
        }

        QuickBarUI.Refresh();
    }

    public void AddItemToQuickBar(Item itemToMove, int slotNumber)
    {
        //QuickBarUI.QuickBar[slotNumber].SetItem(itemToMove);
    }
}
