﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using InControl;

public class Player {

    public int PlayerIndex;
    public PlayerCharacter ActiveCharacter;

    //controls
    public PlayerControllerManager.ConnectedController PlayerController;
    public CharacterActions Controls;
    bool UIOnlyInput;

    //UI
    public UICanvasParent UIParent;

    //Inventory
    public PlayerInventory Inventory;

    public Player(PlayerControllerManager.ConnectedController inputDevice)
    {
        //Setup the player inventory
        Inventory = new PlayerInventory(UIParent.QuickBarUI);

        //Initialize the inventory and visual quickbar
        Inventory.Initialize();
        UIParent.QuickBarUI.InitializeQuickBar(this);

        BindControls(inputDevice);
    }

    public void AddSelectedCharacterToWorld(Vector3 spawnLocation)
    {
        Debug.Log("Adding character");

        ActiveCharacter.OwningPlayer = this;

        //Move the player to the right location in the world
        ActiveCharacter.transform.SetParent(null);
        ActiveCharacter.transform.SetPositionAndRotation(spawnLocation, Quaternion.identity);

        //reenable controls and physics
        BindControls(InputManager.ActiveDevice); // assign to the most recent device pressed - could cause issues in MP
        Controls.AreEnabled = true;
        ActiveCharacter.PhysicsEnabled = true;

        //Set the state of the UI
        UIParent.SetState(UICanvasParent.UIStates.Gameplay);

        //Setup the camera to follow
        GameObject tCamera = GameObject.Find("player" + (PlayerManager.ActivePlayers.Count + 1) + "VCam");
        ActiveCharacter.TrackingCamera = tCamera.GetComponent<Cinemachine.CinemachineVirtualCamera>();
        ActiveCharacter.TrackingCamera.Follow = ActiveCharacter.transform;

        //Set the game state if this is the first player as we dont want to force the state if an additional player
        GameManager.instance.SetState(GameManager.states.Gameplay);

        //refresh the cameras in the case of player count changes
        Debug.Log(PlayerManager.ActivePlayers.Count);
    }

    public void BindControls(InputDevice inputDevice)
    {
        PlayerController.Device = inputDevice;

        Controls = new CharacterActions(PlayerController.Device);
        SetBindings();
    }
    public void BindControls(PlayerControllerManager.ConnectedController connectedController)
    {

        PlayerController = connectedController;

        Controls = new CharacterActions(PlayerController.Device);
        SetBindings();
    }

    void SetBindings()
    {
        Controls.Left.AddDefaultBinding(Key.A);
        Controls.Left.AddDefaultBinding(InputControlType.LeftStickLeft);

        Controls.Right.AddDefaultBinding(Key.D);
        Controls.Right.AddDefaultBinding(InputControlType.LeftStickRight);

        Controls.Up.AddDefaultBinding(Key.W);
        Controls.Up.AddDefaultBinding(InputControlType.LeftStickUp);

        Controls.Down.AddDefaultBinding(Key.S);
        Controls.Down.AddDefaultBinding(InputControlType.LeftStickDown);

        Controls.Interact.AddDefaultBinding(Key.Space);
        Controls.Interact.AddDefaultBinding(InputControlType.Action1);

        Controls.Inventory.AddDefaultBinding(Key.I);
        Controls.Inventory.AddDefaultBinding(InputControlType.Action4);

        Controls.QuickBarRight.AddDefaultBinding(InputControlType.RightBumper);
        Controls.QuickBarLeft.AddDefaultBinding(InputControlType.LeftBumper);
    }

    public void DetectButtonPresses()
    {
        if (UIOnlyInput)
        {
            if (Controls.Inventory.WasPressed) //button 4 command (XB : Y) = Inventory
            {
                if (UIParent.InventoryUI.IsVisible) //if in the Inventory
                {
                    UIParent.SetState(UICanvasParent.UIStates.Gameplay);
                    UIOnlyInput = false;
                }
            }
        }
        else if (!UIOnlyInput)
        {
            if (Controls.Interact.WasPressed) //button 1 command (XB : A) = Interact
            {
                if (ActiveCharacter.GOEquippedItem)
                {
                    if (ActiveCharacter.GOEquippedItem.GetComponent<Item>())
                    {
                        ActiveCharacter.GOEquippedItem.GetComponent<Item>().Use(ActiveCharacter);
                    }
                }
            }

            if (Controls.Inventory.WasPressed) //button 4 command (XB : Y) = Inventory
            {
                UIParent.SetState(UICanvasParent.UIStates.Inventory);
                UIParent.InventoryUI.Refresh(PlayerManager.ActivePlayers.IndexOf(this));
                UIOnlyInput = true;
            }
            if (Controls.QuickBarRight.WasPressed) //Right bumper
            {
                Inventory.QuickBarUI.SelectNext();
            }

            if (Controls.QuickBarLeft.WasPressed) //Left bumper
            {
                Inventory.QuickBarUI.SelectPrevious();
            }

        }
    }

    public void GetMovement()
    {
        //movement
        ActiveCharacter.m_Rigidbody.velocity = new Vector3(Controls.Move.Value.x * Time.deltaTime * ActiveCharacter.MoveSpeed, ActiveCharacter.m_Rigidbody.velocity.y, Controls.Move.Value.y * Time.deltaTime * ActiveCharacter.MoveSpeed);

        //If the character is moving, allow it to rotate during movement to follow the direction of movement.
        if (ActiveCharacter.m_Rigidbody.velocity.magnitude > 1)
        {
            ActiveCharacter.transform.rotation = Quaternion.RotateTowards(ActiveCharacter.transform.rotation, Quaternion.LookRotation(new Vector3(ActiveCharacter.m_Rigidbody.velocity.x, 0, ActiveCharacter.m_Rigidbody.velocity.z)), Time.deltaTime * ActiveCharacter.RotateSpeed);
            ActiveCharacter.ModelAnimator.SetTrigger("Walk");
        }
        else
        {
            ActiveCharacter.ModelAnimator.SetTrigger("Idle");
        }
    }

    public void SetController(InputDevice inputDevice)
    {
        PlayerController.Device = inputDevice;
        BindControls(inputDevice);
        UIParent.NoControllerPopup.IsVisible = false;
    }

    public void SetNoController()
    {
        UIParent.NoControllerPopup.IsVisible = true;
    }

    public void Logout()
    {
        ActiveCharacter.lastHighlightedTile.GetComponent<TileToolTileExt>().Highlight(false);
        UIParent.Cleanup();
        Debug.Log("Logging Off" + ActiveCharacter.name);
        Object.Destroy(ActiveCharacter.gameObject);
    }

    // Update is called once per frame
    private void Update()
    {
        if (Controls != null && Controls.AreEnabled)
        {
            DetectButtonPresses();
        }
        ActiveCharacter.Tick();
    }

    void FixedUpdate()
    {
        if (Controls != null && Controls.AreEnabled)
        {
            if (UIOnlyInput)
            {
                //do stuff for UI only here
            }
            else
            {
                GetMovement();
            }
        }
    }

}
