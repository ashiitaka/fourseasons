﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using InControl;

public class PlayerCharacter : Character {

    public Player OwningPlayer;
    bool physicsEnabled;
    public bool PhysicsEnabled
    {
        get
        {
            return physicsEnabled;
        }
        set
        {
            physicsEnabled = value;
            if (physicsEnabled)
            {
                GetComponent<Rigidbody>().isKinematic = false;
            }
            else
            {
                GetComponent<Rigidbody>().isKinematic = true;
            }


        }
    }

    //view
    public Cinemachine.CinemachineVirtualCamera TrackingCamera;

    //navigation
    GameObject tileHighlightObject;
    public GameObject TileHighlightPrefab;
    public GameObject lastHighlightedTile;

    //interactions
    public GameObject GOEquippedItem;

    public void SpawnPlayerCharacter(Player playerOwner)
    {
        OwningPlayer = playerOwner;

        //Create an instance of the tile highlight object and turn it off
        tileHighlightObject = Instantiate(TileHighlightPrefab);
        tileHighlightObject.SetActive(false);
    }

    public override void Start()
    {
        base.Start();
    }

    void DetectTile()
    {
        RaycastHit hit;

        Debug.DrawLine(new Vector3(transform.position.x, transform.position.y + 1, transform.position.z), new Vector3(transform.forward.x, transform.forward.y - 1.25f, transform.forward.z) * MoveSpeed, Color.red);
        if (Physics.Raycast(new Vector3(transform.position.x, transform.position.y + 1, transform.position.z), new Vector3(transform.forward.x, transform.forward.y - 1.25f, transform.forward.z) * MoveSpeed, out hit))
        {

            if (hit.collider.gameObject.layer == 8)//if in the floortile layer
            {
                GameObject hitTileObject = hit.collider.transform.parent.gameObject;

                if (lastHighlightedTile == null)
                {
                    lastHighlightedTile = hitTileObject;
                }

                if (hitTileObject != lastHighlightedTile)
                {
                    lastHighlightedTile.GetComponent<TileToolTileExt>().Highlight(false);
                    lastHighlightedTile = hitTileObject;
                    lastHighlightedTile.GetComponent<TileToolTileExt>().Highlight(true);
                }

                tileHighlightObject.transform.SetPositionAndRotation(lastHighlightedTile.transform.position, Quaternion.identity);
                tileHighlightObject.SetActive(true);
            }
        }
        else
        {
            tileHighlightObject.SetActive(false);
        }

    }

    public void CollectItem(ComponentManager.ItemDataClass itemData, int quantity)
    {
        OwningPlayer.Inventory.AddItemToInventory(itemData, quantity);
    }

    public void EquipItem(string ItemID)
    {
        Destroy(GOEquippedItem);

        if (ComponentManager.GetItemData(ItemID).ItemID != "None")
        {
            GOEquippedItem = Instantiate(Resources.Load(ComponentManager.GetItemData(ItemID).ModelLink) as GameObject);
            GOEquippedItem.GetComponent<Item>().Data = ComponentManager.GetItemData(ItemID);
            GOEquippedItem.transform.parent = RightHandLocation;
            GOEquippedItem.transform.localPosition = Vector3.zero;
            GOEquippedItem.transform.localEulerAngles = new Vector3(0, 0, 90);
        }
    }

    public void Tick()
    {
        DetectTile();
    }
}
