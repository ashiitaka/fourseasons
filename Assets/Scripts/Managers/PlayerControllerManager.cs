﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using InControl;

public static class PlayerControllerManager {

    public class ConnectedController
    {
        public InputDevice Device;
        public bool IsAssigned;
        public Player AssignedPlayer;

        public ConnectedController(InputDevice inputDevice)
        {
            Device = inputDevice;
        }
    }
    public static List<ConnectedController> ConnectedControllers = new List<ConnectedController>();

    public static void Setup()
    {
        InputManager.OnDeviceAttached += AddDevice;
        InputManager.OnDeviceDetached += RemoveDevice;

        for (int i = 0; i < InputManager.Devices.Count; i++) //add all the currently connected devices
        {
            AddDevice(InputManager.Devices[i]);
        }
    }

    static void AddDevice(InputDevice Device)
    {
        Debug.Log("*************************************************");
        Debug.Log("Adding " + Device.Name);

        for (int i = 0; i < PlayerManager.ActivePlayers.Count; i++) //TODO - need to distinguish if 2 controllers of the same type are connected
        {
            if (PlayerManager.ActivePlayers[i].PlayerController.Device.Name == Device.Name && PlayerManager.ActivePlayers[i].PlayerController.Device.Meta == Device.Meta)
            {
                PlayerManager.ActivePlayers[i].SetController(Device); //resync controller to player - bad ! does by name alone!
            }
        }
        ConnectedControllers.Add(new ConnectedController(Device));
        Debug.Log(string.Concat(Device.Name, " ", Device.Meta));
        Debug.Log("Connected Controllers: " + ConnectedControllers.Count);
        Debug.Log("*************************************************");

    }

    static void RemoveDevice(InputDevice Device)
    {
        Debug.Log("*************************************************");
        Debug.Log(string.Concat("Removing ", Device.Name, " ", Device.Meta));
        for (int i = 0; i < PlayerManager.ActivePlayers.Count; i++)
        {
            if(PlayerManager.ActivePlayers[i].PlayerController.Device.Name == Device.Name && PlayerManager.ActivePlayers[i].PlayerController.Device.Meta == Device.Meta)
            {
                PlayerManager.ActivePlayers[i].SetNoController();
            }
        }

        ConnectedControllers.Clear(); // clear out all references as Unity will add them all again anyway... thanks
        Debug.Log("*************************************************");
    }

    public static void MonitorControls()
    {
        if (InputManager.ActiveDevice.CommandWasPressed && GameManager.GameState == GameManager.states.FrontEnd)
        {
            if (PlayerManager.ActivePlayers.Count < 4)
            {
                for (int i = 0; i < ConnectedControllers.Count; i++)
                {
                    if (ConnectedControllers[i].Device == InputManager.ActiveDevice && !ConnectedControllers[i].IsAssigned)
                    {
                        ConnectedControllers[i].IsAssigned = true;
                        PlayerManager.instance.AddNewPlayer(ConnectedControllers[i]);
                    }
                }

            }
        }
    }
}
