﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioManager : MonoBehaviour {

    public static AudioManager instance;

    [System.Serializable]
    public class backingTracks
    {
        public AudioClip MainLoop;
        public AudioClip CasualLoop;
    }

    [System.Serializable]
    public class SFXClips
    {
        [Header("Farming Sounds")]
        public AudioClip ToolSwing;
        public AudioClip HoeGround;

        [Header("SFX")]
        public AudioClip ButtonSuccess;

        [Header("Stings")]
        public AudioClip AchievementUnlocked;
    }

    public AudioSource MusicPlayer;
    List<AudioSource> SFXPlayer = new List<AudioSource>();

    [Header("Mixers")]
    public AudioMixerGroup BackingMusicMixer;
    public AudioMixerGroup SFXMixer;

    [Header("Background Music")]
    public backingTracks BackingTracks;

    [Header("SFX")]
    public bool IsSFXMuted;
    public SFXClips Sounds;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        //If instance already exists and it's not this:
        else if (instance != this)
        {
            //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
            Destroy(gameObject);
        }

        MusicPlayer = gameObject.AddComponent<AudioSource>();
        MusicPlayer.outputAudioMixerGroup = BackingMusicMixer;
        MusicPlayer.loop = true;

        SFXPlayer.Add(gameObject.AddComponent<AudioSource>()); ;
        SFXPlayer[0].outputAudioMixerGroup = SFXMixer;
        ChangeBackingMusic(BackingTracks.MainLoop);

        IsSFXMuted = false;
    }

    public void PlaySFX(AudioClip audioToPlay)
    {
        if(!IsSFXMuted)
        {
            bool hasPlayed = false;

            for (int i = 0; i < SFXPlayer.Count; i++)
            {
                if (!SFXPlayer[i].isPlaying && !hasPlayed)
                {
                    SFXPlayer[i].clip = audioToPlay;
                    SFXPlayer[i].Play();
                    hasPlayed = true;
                }
            }

            if (!hasPlayed)
            {
                SFXPlayer.Add(this.gameObject.AddComponent<AudioSource>());
                int index = SFXPlayer.Count - 1;
                SFXPlayer[index].outputAudioMixerGroup = SFXMixer;
                SFXPlayer[index].clip = audioToPlay;
                SFXPlayer[index].Play();
            }
        }  
    }

    public void ChangeBackingMusic(AudioClip newTrack)
    {
        if(MusicPlayer.clip != newTrack)
        {
            MusicPlayer.clip = newTrack;
            MusicPlayer.Play();
        }
    }

    public void ToggleMusic()
    {
        bool b = MusicPlayer.mute;
        MusicPlayer.mute = !b;        
    }

    public void ToggleSFX()
    {
        for (int i = 0; i < SFXPlayer.Count; i++)
        {
            SFXPlayer[i].mute = !IsSFXMuted;
        }

        IsSFXMuted = !IsSFXMuted;
    }
}
