﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using Ideafixxxer.CsvParser;
using UnityEngine;

public class ComponentManager {

    static CsvParser csvparser;

    public static bool DataLoaded;


    enum dataTypes
    {
        Heads,
        Bodies,
        Items
    }

    public static List<GameObject> Heads = new List<GameObject>();
    public static List<GameObject> Bodies = new List<GameObject>();

    public static List<ItemDataClass> Items = new List<ItemDataClass>();
    [System.Serializable]
    public class ItemDataClass
    {
        public string ItemID;
        public string Name;
        public string ModelLink;
        public string ItemType;
    }

    public static bool LoadComponentData()
    {
        csvparser = new CsvParser();
        DataLoaded = false;

        ReadCSVDataFile(dataTypes.Heads);
        ReadCSVDataFile(dataTypes.Bodies);

        ReadCSVDataFile(dataTypes.Items);

        DataLoaded = true;
        return true;
    }

    static void ReadCSVDataFile(dataTypes dataType)
    {
        IList<Object> tempList = new List<Object>();
        IList<ItemDataClass> tempItemList = new List<ItemDataClass>();

        string csvData = File.ReadAllText("Data/" + dataType + ".csv");
        string[][] parsedCSV = csvparser.Parse(new StringReader(csvData));

        for (int i = 1; i < parsedCSV.LongLength; i++) //Row number
        {
            //ID //name //model
            Debug.Log(string.Concat(parsedCSV[i][0], " ", parsedCSV[i][1], " ", parsedCSV[i][2]));
            if(dataType == dataTypes.Bodies || dataType == dataTypes.Heads)
            {
                tempList.Add(Resources.Load("Characters/" + dataType + "/" + parsedCSV[i][2], typeof(Object)));
            }

            if(dataType == dataTypes.Items)
            {
                ItemDataClass temp = new ItemDataClass();
                temp.ItemID = parsedCSV[i][0];
                temp.Name = parsedCSV[i][1];
                temp.ModelLink = parsedCSV[i][2];
                temp.ItemType = parsedCSV[i][3];
                tempItemList.Add(temp);
            }

        }

        for (int i = 0; i < tempList.Count; i++)
        {
            switch (dataType)
            {
                case dataTypes.Heads:
                    Heads.Add(tempList[i] as GameObject);
                    break;
                case dataTypes.Bodies:
                    Bodies.Add(tempList[i] as GameObject);
                    break;
                default:
                    break;
            }
            
        }

        for (int i = 0; i < tempItemList.Count; i++)
        {
            switch (dataType)
            {
                case dataTypes.Items:
                    Items.Add(tempItemList[i] as ItemDataClass);
                    break;
                default:
                    break;
            }

        }

        tempList.Clear();
        tempItemList.Clear();
    }

    public static ItemDataClass GetItemData(string ItemID)
    {
        for (int i = 0; i < Items.Count; i++)
        {
            if(Items[i].ItemID == ItemID)
            {
                return Items[i];
            }
        }
        return Items[0];
    }
}
