﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class DataManager
{
    public static void SavePlayerCharacter(string player, string Character)
    {
        ES3.Save<string>(player,player); // save the player

        ES3.Save<string>(player + "0" + "Name", Character);
        ES3.Save<int>(player + "0" + "Head", 1);
        ES3.Save<int>(player + "0" + "Body", 2);
    }

    public static void LoadPlayerCharacters(string player)
    {

    }

    public static void LoadData()
    {
        // Load our position and rotation when this script loads.
        // Or return default values if there's no data to load.
        //transform.position = ES3.Load<Vector3>("myPosition", Vector3.zero);
        //transform.rotation = ES3.Load<Quaternion>("myRotation", Quaternion.identity);
    }
}
