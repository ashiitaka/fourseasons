﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour {

    public static UIManager instance;

    public static UICanvasParent UIParent;
    public UITitleScreen TitleScreenUI;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }

        //If instance already exists and it's not this:
        else if (instance != this)
        {
            //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
            Destroy(gameObject);
        }

        GameObject t = Resources.Load("UICanvas") as GameObject;
        UIParent = t.GetComponent<UICanvasParent>();
    }

}
