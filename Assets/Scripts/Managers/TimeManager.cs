﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeManager : MonoBehaviour {

    public static TimeManager instance;

    [Header("Day / Night Cycle")]
    public bool Override;
    public float SunlightYPivot = 130;
    public GameObject SunlightParent;

    public string Date;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        //If instance already exists and it's not this:
        else if (instance != this)
        {
            //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
            Destroy(gameObject);
        }
    }

    // Use this for initialization
    void Start ()
    {
        
	}
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        Date = System.DateTime.UtcNow.ToString();
        if(!Override)
        {
            SunlightParent.transform.localRotation = Quaternion.Euler(((360 / 24) * System.DateTime.Now.Hour) + (System.DateTime.Now.Minute * 0.25f) + (System.DateTime.Now.Second * 0.004f), SunlightYPivot, 0);
        }        
    }
}
