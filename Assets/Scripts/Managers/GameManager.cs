﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using InControl;

public class GameManager : MonoBehaviour {

    //Statics
    public static GameManager instance;
    public static states GameState;

    //States
    public GameObject FrontEnd;

    //Constants
    public const int MaxPlayers = 4;
    public const int MaxSavedCharacters = 4;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        //If instance already exists and it's not this:
        else if (instance != this)
        {
            //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
            Destroy(gameObject);
        }
    }

    // Use this for initialization
    void Start ()
    {
        StartCoroutine(LoadAllData());
        PlayerControllerManager.Setup(); //setup the controller manager
    }

    void OnApplicationPause(bool state) //apparently called on mobile ?
    {
        if (state)
        {
            OnApplicationQuit(); //save if "paused"
        }
    }

    void OnApplicationQuit()
    {
        DataManager.SavePlayerCharacter("PlayerName", "Bob");
        Debug.Log("Quitting");
    }
    IEnumerator LoadAllData()
    {
        SetState(states.Loading);

        while (!ComponentManager.LoadComponentData()) //load in all the components - might need a delay here ?!
        {
            Debug.Log("Loading");
        }

        DataManager.LoadData();

        yield return new WaitForSeconds(0);
        SetState(states.FrontEnd);        
    }

    public void SetState(states newState)
    {
        GameState = newState;

        switch (GameState)
        {
            case states.Loading:
                {
                    FrontEnd.SetActive(true);
                    UIManager.instance.TitleScreenUI.IsVisible = true;
                    AudioManager.instance.ChangeBackingMusic(AudioManager.instance.BackingTracks.MainLoop);
                    UIManager.instance.TitleScreenUI.SetState(UITitleScreen.states.Loading);
                }
                break;
            case states.FrontEnd:
                {
                    UIManager.instance.TitleScreenUI.SetState(UITitleScreen.states.Loaded);
                }
                break;
            case states.Gameplay:
                {
                    FrontEnd.SetActive(false);
                    AudioManager.instance.ChangeBackingMusic(AudioManager.instance.BackingTracks.CasualLoop);
                }
                break;
            default:
                break;
        }
    }

    public enum states
    {
        Loading,
        FrontEnd,
        Gameplay
    }

}
