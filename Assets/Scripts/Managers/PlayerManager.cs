﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using InControl;

public class PlayerManager : MonoBehaviour {

    public static PlayerManager instance;

    public PlayerCharacter PlayerCharacterPrefab;

    //Active Player list
    public static List<Player> ActivePlayers = new List<Player>();

    //Game Cameras
    public class PlayerCamera
    {
        public Camera MainCamera;
        public Camera UICamera;
        public List<Cinemachine.CinemachineVirtualCamera> VirtualCameras = new List<Cinemachine.CinemachineVirtualCamera>();

        public PlayerCamera(Camera mainCam)
        {
            MainCamera = mainCam;
        }

    }

    public static List<PlayerCamera> PlayerCameras = new List<PlayerCamera>();

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }

        //If instance already exists and it's not this:
        else if (instance != this)
        {
            //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
            Destroy(gameObject);
        }

    }

    private void Start()
    {
        for (int i = 0; i < GameManager.MaxPlayers; i++)
        {
            PlayerCameras.Add(new PlayerCamera(GameObject.Find("Player" + (i + 1) + "Camera").GetComponent<Camera>()));
        }

        for (int i = 0; i < PlayerCameras.Count; i++)
        {
            PlayerCameras[i].UICamera = GameObject.Find("Player" + (i + 1) + "UICamera").GetComponent<Camera>(); ;
        }

        SetupCameras(0);
    }

    void Update()
    {
        PlayerControllerManager.MonitorControls();
    }

    public void AddNewPlayer(PlayerControllerManager.ConnectedController connectedController)
    {
        ActivePlayers.Add(new Player(connectedController));

        //setup the cameras for +1 of our active players
        SetupCameras(ActivePlayers.Count);

        ActivePlayers[ActivePlayers.Count - 1].PlayerController = connectedController; // set the input device
        //connectedController.AssignedPlayer = tempPlayer; // assign this player to this controller - do I need to do this twice?

        AddPlayerUI(ActivePlayers[ActivePlayers.Count - 1]);

    }

    public void AddPlayerUI(Player player)
    {
        player.UIParent = Instantiate(UIManager.UIParent) as UICanvasParent;

        player.UIParent.GetComponent<Canvas>().worldCamera = PlayerCameras[ActivePlayers.Count].UICamera;
        player.UIParent.GetComponent<Canvas>().planeDistance = 10;
        player.UIParent.GetComponent<UnityEngine.UI.CanvasScaler>().scaleFactor = 1.0f / (ActivePlayers.Count + 1);
        SetCameraScales(1);

        //handle any UI changes
        UIManager.instance.TitleScreenUI.IsVisible = false;
        player.UIParent.gameObject.SetActive(true);
        player.UIParent.SetState(UICanvasParent.UIStates.CharacterSelect);
    }

    public void ShowCharacterCreation(Player controllingPlayer)
    {
        controllingPlayer.UIParent.SetState(UICanvasParent.UIStates.CharacterCustomization);
        CharacterCreation.instance.ShowCharacterCreation(controllingPlayer,null, 0, 0);
    }

    public void RemovePlayer(Player playerToRemove)
    {
        ActivePlayers.Remove(playerToRemove);
        playerToRemove.Logout();

        SetupCameras(ActivePlayers.Count);
    }

    void SetupCameras(int viewCount)
    {
        switch (viewCount)
        {
            case 0:
            case 1:
                {
                    PlayerCameras[0].MainCamera.rect = new Rect(new Vector2(0, 0), new Vector2(1, 1));
                    PlayerCameras[1].MainCamera.gameObject.SetActive(false);
                    PlayerCameras[2].MainCamera.gameObject.SetActive(false);
                    PlayerCameras[3].MainCamera.gameObject.SetActive(false);
                    break;
                }
            case 2:
                {
                    PlayerCameras[0].MainCamera.rect = new Rect(new Vector2(0, 0), new Vector2(.5f, 1));
                    PlayerCameras[1].MainCamera.gameObject.SetActive(true);
                    PlayerCameras[1].MainCamera.rect = new Rect(new Vector2(.5f, 0), new Vector2(.5f, 1));
                    PlayerCameras[2].MainCamera.gameObject.SetActive(false);
                    PlayerCameras[3].MainCamera.gameObject.SetActive(false);
                    break;
                }
            case 3:
                {
                    PlayerCameras[0].MainCamera.rect = new Rect(new Vector2(0, 0), new Vector2(.5f, .5f));
                    PlayerCameras[1].MainCamera.gameObject.SetActive(true);
                    PlayerCameras[1].MainCamera.rect = new Rect(new Vector2(.5f, 0), new Vector2(.5f, .5f));
                    PlayerCameras[2].MainCamera.gameObject.SetActive(true);
                    PlayerCameras[2].MainCamera.rect = new Rect(new Vector2(0f, .5f), new Vector2(1f, .5f));
                    PlayerCameras[3].MainCamera.gameObject.SetActive(false);
                    break;
                }
            case 4:
                {
                    PlayerCameras[0].MainCamera.rect = new Rect(new Vector2(0, 0), new Vector2(.5f, .5f));
                    PlayerCameras[1].MainCamera.gameObject.SetActive(true);
                    PlayerCameras[1].MainCamera.rect = new Rect(new Vector2(.5f, 0), new Vector2(.5f, .5f));
                    PlayerCameras[2].MainCamera.gameObject.SetActive(true);
                    PlayerCameras[2].MainCamera.rect = new Rect(new Vector2(0, .5f), new Vector2(.5f, .5f));
                    PlayerCameras[3].MainCamera.gameObject.SetActive(true);
                    PlayerCameras[3].MainCamera.rect = new Rect(new Vector2(.5f, .5f), new Vector2(.5f, .5f));
                    break;
                }

            default:
                break;
        }
        SynchronizeUICameras();
        SetCameraScales(0);
    }

    void SetCameraScales(int playerVariance)
    {
        for (int i = 0; i<ActivePlayers.Count; i++)
        {
            ActivePlayers[i].UIParent.GetComponent<UnityEngine.UI.CanvasScaler>().scaleFactor = 1.0f / (ActivePlayers.Count + playerVariance);
        }
    }

    void SynchronizeUICameras()
    {
        for (int i = 0; i < PlayerCameras.Count; i++) //match the rect of the UI Cameras to that of the main cameras
        {
            PlayerCameras[i].UICamera.rect = PlayerCameras[i].MainCamera.rect;
        }
    }
}
