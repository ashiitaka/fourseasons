﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UICanvasParent : MonoBehaviour {

    public enum UIStates
    {
        Gameplay,
        CharacterSelect,
        CharacterCustomization,
        Inventory,
    }

    public int PlayerOwnerIndex;

    [Header("Groups")]
    public GUIGroup CharacterSelect;
    public GUIGroup CharacterCustomization;
    public GUIGroup GameplayMaster;
    public InventoryDisplay InventoryUI;

    [Header("Specific Children")]
    public InventoryBar QuickBarUI;

    [Header("Popups")]
    public GUIGroup NoControllerPopup;

    public UICanvasParent(UIStates newState)
    {
        SetState(UIStates.Gameplay);
    }

    public void SetState(UIStates newState)
    {
        CloseAllUIs();

        switch (newState)
        {
            case UIStates.Gameplay:
                {
                    GameplayMaster.IsVisible = true;
                }
                break;
            case UIStates.CharacterSelect:
                {
                    CharacterSelect.IsVisible = true;
                }
                break;
            case UIStates.CharacterCustomization:
                {
                    CharacterCustomization.IsVisible = true;
                }
                break;
            case UIStates.Inventory:
                {
                    InventoryUI.IsVisible = true;
                }
                break;
            default:
                break;
        }
    }

    private void CloseAllUIs()
    {
        GameplayMaster.IsVisible = false;
        CharacterSelect.IsVisible = false;
        CharacterCustomization.IsVisible = false;
        InventoryUI.IsVisible = false;
        NoControllerPopup.IsVisible = false;
    }

    public void Cleanup()
    {
        Destroy(this.gameObject);
    }
}
