﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryDisplay : GUIGroup {

    // The inventory is a display of all slots ON TOP of the quickbar
    // Therefore if the quickkbar has 10 items
    // and the Max inventory is 30 items
    // there will be 20 slots in the inventory UI. (the latter 20 slots)

    public GridLayoutGroup ItemGrid;

    List<InventorySlot> iSlots = new List<InventorySlot>();
    public InventorySlot InventorySlotTemplate;

    public override void OnEnable()
    {
        if(iSlots.Count < PlayerInventory.MaxInventorysize - PlayerInventory.QuickBarSize) // only add the slots on top of the quickbar size
        {
            for (int i = 0; i < (PlayerInventory.MaxInventorysize - PlayerInventory.QuickBarSize); i++)
            {
                iSlots.Add(Instantiate(InventorySlotTemplate, ItemGrid.transform));
                iSlots[i].SetNavigationType(Navigation.Mode.Automatic);
                iSlots[i].PlayerInventoryIndex = i + PlayerInventory.QuickBarSize;
            }
        }
    }

    public void Refresh(int playerIndex)
    {
        for (int i = 0; i < (PlayerInventory.MaxInventorysize - PlayerInventory.QuickBarSize); i++)
        {
            iSlots[i].SetItem(PlayerManager.ActivePlayers[playerIndex], i + PlayerInventory.QuickBarSize);
        }

        iSlots[0].SelectItem(); // select the first visible item in the slots
    }
}
