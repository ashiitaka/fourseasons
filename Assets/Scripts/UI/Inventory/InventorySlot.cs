﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class InventorySlot : MonoBehaviour {

    public TMPro.TextMeshProUGUI ItemName;
    public TMPro.TextMeshProUGUI ItemQuantity;

    Player PlayerOwner;
    public int PlayerInventoryIndex;

    public bool AutomaticControllerNavigationEnabled;

    public RawImage ItemImageRenderTexture;
    RenderTexture renderTextureInstance;

    public UIAssetPreview assetPreview;

    Button button;

    private void Awake()
    {
        renderTextureInstance = Instantiate(assetPreview.ItemPreviewRenderTextureMaster);
        ItemImageRenderTexture.texture = renderTextureInstance;
        assetPreview.PreviewCamera.targetTexture = renderTextureInstance;

        button = GetComponent<Button>();

    }

    public void SetNavigationType(Navigation.Mode newMode)
    {
        Navigation tempnavigation = button.navigation;
        tempnavigation.mode = newMode;
        button.navigation = tempnavigation;
    }
    public void SetItem(Player playerRef, int InventoryIndex)
    {
        PlayerOwner = playerRef;

        ComponentManager.ItemDataClass tempItem = ComponentManager.GetItemData(PlayerOwner.Inventory.GetItemList()[InventoryIndex].ItemID);

        ItemName.text = tempItem.Name;
        if(PlayerOwner.Inventory.GetItemList()[InventoryIndex].Quantity > 0)
        {
            ItemQuantity.text = PlayerOwner.Inventory.GetItemList()[InventoryIndex].Quantity.ToString();
        }
        else
        {
            ItemQuantity.text = "";
        }

        assetPreview.ChangeItem(Resources.Load(tempItem.ModelLink) as GameObject);
    }
       
    public void SelectItem()
    {
        button.Select();
        if(PlayerOwner != null)
        {
            PlayerOwner.ActiveCharacter.EquipItem(PlayerOwner.Inventory.GetItemList()[PlayerInventoryIndex].ItemID);
        }
        Debug.Log(EventSystem.current.currentSelectedGameObject);
    }

    private void Update()
    {
        if(EventSystem.current.currentSelectedGameObject == this.gameObject)
        {
            transform.localScale = new Vector3(1.1f, 1.1f, 1);
        }
        else
        {
            transform.localScale = Vector3.one;
        }
    }
}
