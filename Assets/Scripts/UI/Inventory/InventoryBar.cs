﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryBar : MonoBehaviour {

    public Transform GridParent;
    public InventorySlot InventorySlotTemplate;

    public List<InventorySlot> QuickBar = new List<InventorySlot>();
    int selectedIndex;

    Player localPlayer;
    
    public void InitializeQuickBar(Player player)
    {
        localPlayer = player;

        for (int i = 0; i < PlayerInventory.QuickBarSize; i++)
        {
            QuickBar.Add(Instantiate(InventorySlotTemplate, GridParent));
            QuickBar[i].SetNavigationType( UnityEngine.UI.Navigation.Mode.None);
            QuickBar[i].PlayerInventoryIndex = i;
            QuickBar[i].SetItem(localPlayer, i);
        }
    }

    public void SelectSlot(int slotIndex)
    {
        if(slotIndex >= QuickBar.Count)
        {
            selectedIndex = 0;
        }
        else if(slotIndex < 0)
        {
            selectedIndex = QuickBar.Count -1;
        }
        else
        {
            selectedIndex = slotIndex;
        }

        Debug.Log("Selected Index = " + +selectedIndex);
        QuickBar[selectedIndex].SelectItem();        
    }

    public void SelectNext()
    {
        selectedIndex++;
        SelectSlot(selectedIndex);
    }

    public void SelectPrevious()
    {
        selectedIndex--;
        SelectSlot(selectedIndex);
    }

    public void Refresh()
    {
        for (int i = 0; i < PlayerInventory.QuickBarSize; i++)
        {
            QuickBar[i].SetItem(localPlayer, i);
        }
    }
}
