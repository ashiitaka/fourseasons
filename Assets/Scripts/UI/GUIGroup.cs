﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GUIGroup : MonoBehaviour {

    [SerializeField]
    bool isVisible;
    public bool IsVisible
    {
        get
        {
            return isVisible;
        }
        set
        {
            isVisible = value;
            if (value)
            {
                gameObject.SetActive(value);
            }

            if (anims != null && anims.isActiveAndEnabled)
            {
                if (value && EnableAnimation != "")
                {
                    anims.SetTrigger(EnableAnimation);
                }
                else if (!value && DisableAnimation != "")
                {
                    anims.SetTrigger(DisableAnimation);
                }
            }
            else if (!value)
            {
                gameObject.SetActive(value);
            }
        }
    }

    public string EnableAnimation;
    public string DisableAnimation;

    public Selectable DefaultControllerSelection;

    Animator anims;

    private void Awake()
    {
        if(this.GetComponent<Animator>())
        {
            anims = GetComponent<Animator>();
        }
    }

    public virtual void OnEnable()
    {
        if(DefaultControllerSelection != null)
        {
            /*
            if(!DefaultControllerSelection.isActiveAndEnabled)
            {
                if (DefaultControllerSelection.FindSelectable(Vector2.right) != null)
                {
                    DefaultControllerSelection = DefaultControllerSelection.FindSelectable(Vector2.right);
                }
                if (DefaultControllerSelection.FindSelectable(Vector2.down) != null)
                {
                    DefaultControllerSelection = DefaultControllerSelection.FindSelectable(Vector2.down);
                }
            }
            */
            Utilities.SelectObject(DefaultControllerSelection.gameObject);
        }
    }

    private void Update()
    {
        if(anims != null)
        {
            if (anims.GetCurrentAnimatorStateInfo(0).IsName("Disabled"))
            {
                gameObject.SetActive(false);
            }
        }

    }
}
