﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UITitleScreen : GUIGroup {

    public GameObject LoadingState;
    public GameObject LoadedState;

    public enum states
    {
        Loading,
        Loaded
    }
	// Use this for initialization
	void Start () {
        SetState(states.Loading);
	}

    public void SetState(states newState)
    {
        LoadingState.SetActive(false);
        LoadedState.SetActive(false);

        switch (newState)
        {
            case states.Loading:
                LoadingState.SetActive(true);
                break;
            case states.Loaded:
                LoadedState.SetActive(true);
                break;
            default:
                break;
        }
    }
}
