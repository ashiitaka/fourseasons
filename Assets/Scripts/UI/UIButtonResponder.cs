﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class UIButtonResponder : MonoBehaviour, ISelectHandler, IDeselectHandler {

    Image buttonImage;

    public enum Function
    {
        PreviousHead,
        NextHead,
        PreviousBody,
        NextBody,
        CharacterCreationConfirm,
        CreateNewCharacter,
        LoadCharacter,
    }

    public Function ButtonFunction;

    private void Awake()
    {
        Button button = GetComponent<Button>();
        buttonImage = GetComponent<Image>();

        button.onClick.AddListener(OnPress);
    }

    public void OnSelect(BaseEventData eventData)
    {
        Selected();
    }

    void Selected()
    {
        
    }

    public void OnDeselect(BaseEventData eventData)
    {
        
    }

    public void OnPress()
    {
        switch (ButtonFunction)
        {
            case Function.PreviousHead:
                CharacterCreation.instance.ComponentCycle(CharacterCreation.componentType.Head, CharacterCreation.NextOrPrevious.Previous);
                break;
            case Function.NextHead:
                CharacterCreation.instance.ComponentCycle(CharacterCreation.componentType.Head, CharacterCreation.NextOrPrevious.Next);
                break;
            case Function.PreviousBody:
                CharacterCreation.instance.ComponentCycle(CharacterCreation.componentType.Body, CharacterCreation.NextOrPrevious.Previous);
                break;
            case Function.NextBody:
                CharacterCreation.instance.ComponentCycle(CharacterCreation.componentType.Body, CharacterCreation.NextOrPrevious.Next);
                break;
            case Function.CharacterCreationConfirm:
                CharacterCreation.instance.ConfirmCharacterCreation(PlayerManager.ActivePlayers[0]);
                break;
            case Function.CreateNewCharacter:
                PlayerManager.instance.ShowCharacterCreation(PlayerManager.ActivePlayers[0]); // not nice probably wont work with more players
                break;
            case Function.LoadCharacter:
                PlayerManager.ActivePlayers[0].AddSelectedCharacterToWorld(new Vector3(0, .5f, 0));
                //PlayerManager.instance.AddPlayer(PlayerManager.ActivePlayers[0], new Vector3(0, .5f, 0));
                break;
            default:
                break;
        }
    }
}
