﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class UIAssetPreview {

    public const int ItemPreviewLayer = 15;

    public Camera PreviewCamera;
    public RenderTexture ItemPreviewRenderTextureMaster;

    GameObject tempItem;
    GameObject itemDisplayModel;

    public void ChangeItem(GameObject newModel)
    {
        if(newModel != null)
        { 
            if (itemDisplayModel != null)
            {
                Object.Destroy(itemDisplayModel);
            }

            if (newModel != null)
            {
                itemDisplayModel = Object.Instantiate(newModel, PreviewCamera.transform);
                itemDisplayModel.transform.localScale = new Vector3(100, 100, 100);
                itemDisplayModel.transform.localRotation = Quaternion.identity;
                itemDisplayModel.transform.localPosition = new Vector3(0, 0, 30);

                itemDisplayModel.gameObject.layer = ItemPreviewLayer;
                for (int i = 0; i < itemDisplayModel.transform.childCount; i++)
                {
                    Transform t = itemDisplayModel.transform.GetChild(i);
                    t.gameObject.layer = ItemPreviewLayer;
                }
                
            }
        }
    }
}
