﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DynamicGrid : MonoBehaviour  {

    public enum gridTypes
    {
        CharacterSelection,
    }

    public GameObject HeaderTemplate;
    public GameObject Template;
    public gridTypes GridType;
    public List<GameObject> IgnoredObjects = new List<GameObject>();

    List<GameObject> ChildObjects = new List<GameObject>();
    bool _selectedFirstEntry;

    void OnEnable ()
    {
        _selectedFirstEntry = false;
        if(Template!=null)
        {
            Template.SetActive(false);
        }

        if(HeaderTemplate != null)
        {
            HeaderTemplate.SetActive(false);
        }

        foreach (Transform t in this.transform)
        {
            if(t.gameObject.activeInHierarchy)
            {
                if(!IgnoredObjects.Contains(t.gameObject))
                {
                    Destroy(t.gameObject);
                }                
            }
        }
        switch (GridType)
        {
            case gridTypes.CharacterSelection:
                if(ES3.KeyExists("PlayerName")) // check to see if the player exists
                {
                    for (int i = 0; i < GameManager.MaxSavedCharacters; i++)
                    {
                        if(ES3.KeyExists("PlayerName"+i+"Name"))
                        {
                            GameObject t = Instantiate(Template, this.transform);
                            t.GetComponent<VariableObject>().Index = i;
                            t.GetComponentInChildren<TMPro.TextMeshProUGUI>().text = ES3.Load<string>("PlayerName" + i + "Name");
                            t.SetActive(true);
                            t.transform.SetAsLastSibling();
                            ChildObjects.Add(t);
                        }
                    }
                }
                if(IgnoredObjects[0].activeInHierarchy)
                {
                    SelectFirstEntry(IgnoredObjects[1]); //default to new character
                }
                else //if we are on max characters, select the top one
                {
                    SelectFirstEntry(ChildObjects[0]);
                }
                break;
            default:
                break;

        }

    }

    void SelectFirstEntry(GameObject selection)
    {
        Utilities.SelectObject(selection);
        _selectedFirstEntry = true;
    }

}
