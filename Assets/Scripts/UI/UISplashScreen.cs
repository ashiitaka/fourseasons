﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;

public class UISplashScreen : MonoBehaviour {

	// Use this for initialization
	void Start ()
    {
        StartCoroutine(LoadDelay());        
	}

    IEnumerator LoadDelay()
    {
        yield return new WaitForSeconds(5);
        LoadingSceneManager.LoadScene("Main");
    }
	
}
