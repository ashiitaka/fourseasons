﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UITimeDisplay : MonoBehaviour {

    public TMPro.TextMeshProUGUI TimeAndDate;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        TimeAndDate.text = TimeManager.instance.Date;
	}
}
