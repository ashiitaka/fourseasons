﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComponentBase : MonoBehaviour {

    public GameObject MainObject;
    public List<Texture> Textures = new List<Texture>();
    public List<MeshRenderer> ChildObjects = new List<MeshRenderer>();

    public void UpdateTextures(int textureIndex)
    {
        MainObject.GetComponent<Renderer>().material.mainTexture = Textures[textureIndex];

        for (int i = 0; i < ChildObjects.Count; i++)
        {
            ChildObjects[i].material.mainTexture = Textures[textureIndex];
        }
    }

}
