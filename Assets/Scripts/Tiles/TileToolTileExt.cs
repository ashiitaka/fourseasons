﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TileInteractionType
{
    Hoe,
}

public class TileToolTileExt : MonoBehaviour {

    TileToolTile tile;
    MeshRenderer meshMaterial;

    public enum TileType
    {
        Default,
        Dirt,
    }

    public TileType Type;

    private void Awake()
    {
        tile = this.GetComponent<TileToolTile>();
        meshMaterial = GetComponentInChildren<MeshRenderer>();
    }

    public void Highlight(bool state)
    {
        if(state)
        {
            //this tile is selected
        }
        else
        {
            //this tile is not selected
        }
    }

    public void Interact(TileInteractionType interactionType)
    {
        switch (interactionType)
        {
            case TileInteractionType.Hoe:
                if(Type == TileType.Default)
                {
                    SwitchTile(TileType.Dirt);
                }
                break;
            default:
                break;
        }
    }

    void SwitchTile(TileType tileType)
    {
        Type = tileType;

        switch (tileType)
        {
            case TileType.Default:
                break;
            case TileType.Dirt:
                GameObject i = Instantiate(TileManager.instance.DirtTile);
                i.transform.SetParent(this.transform.parent);
                i.transform.SetPositionAndRotation(transform.position,Quaternion.identity);
                Instantiate(Resources.Load("Effects/DirtParticle", typeof(GameObject)), i.transform); //TODO - get a pool of particles somewhere
                Destroy(this.gameObject);
                break;
            default:
                break;
        }
    }
}
