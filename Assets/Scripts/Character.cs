﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour {

    //Asset Setup
    public string Name = "Name";
    public int UID;
    public Animator ModelAnimator;
    public Rigidbody m_Rigidbody;

    public float MoveSpeed = 1;
    public float RotateSpeed = 1;

    public SkinnedMeshRenderer BodyMeshRenderer;
    public Transform HeadLocation;
    public Transform BodyLocation;
    public Transform RightHandLocation;

    public Bodycomposition CharacterComposition = new Bodycomposition();

    public class Bodycomposition
    {
        public GameObject Head;
        public int HeadIndex;
        public int HeadTextureIndex;
        public GameObject Body;
        public int BodyIndex;
        public int BodyTextureIndex;
        public GameObject RightHand;
    }

    //use override to add extra specific functions to the parent method
    public virtual void Start()
    {
        //Fetch the Rigidbody component you attach from your GameObject
        m_Rigidbody = GetComponent<Rigidbody>();
    }

    public void RefreshBodyComposition(Bodycomposition newBody)
    {
        CharacterComposition = newBody;

        if (CharacterComposition.Head != null)
        {
            Destroy(CharacterComposition.Head);
        }
        if(CharacterComposition.Body != null)
        {
            Destroy(CharacterComposition.Body);
        }        

        CharacterComposition.Head = Instantiate(ComponentManager.Heads[CharacterComposition.HeadIndex], HeadLocation);
        CharacterComposition.Head.GetComponent<ComponentBase>().UpdateTextures(CharacterComposition.HeadTextureIndex);
        CharacterComposition.Body = Instantiate(ComponentManager.Bodies[CharacterComposition.BodyIndex], BodyLocation);
        CharacterComposition.Body.GetComponent<ComponentBase>().UpdateTextures(CharacterComposition.BodyTextureIndex);
    }

}
